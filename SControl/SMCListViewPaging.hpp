#pragma once
#include "ListKernel.h"
#include "ListKernel.cpp"
#include "SPaging.h"

template <typename T, typename M>
class SMCListViewPaging :public CListKernel<T, M>
{
public:
	SMCListViewPaging() :m_ListCount(10)
	{
	}
	~SMCListViewPaging()
	{
	}
	void push_back(T &p)
	{
		m_Data.push_back(p);
	}
	void UpdateView(int Sel=0)
	{
		m_pSPaging->SetPageCount(m_Data.size() % m_ListCount==0?m_Data.size() / m_ListCount:m_Data.size()/m_ListCount+1);
		UpdataListShow(Sel);
		__super::UpdateView();
	}

	void clear(bool IsUpdateView = false)
	{
		m_Data.clear();
		if (IsUpdateView) UpdateView();
	}

	typename std::list<T>::iterator begin(void)
	{
		return m_Data.begin();
	}

	typename std::list<T>::iterator end(void)
	{
		m_Data.end();
	}
	//获取原始数据列表
	std::list<T>& GetList(void)
	{
		return m_Data;
	}
	//设置列表
	void SetList(std::list<T>& lists, bool IsUpdateView = false)
	{
		m_Data = lists;
		if (IsUpdateView) UpdateView();
	}
	void Init(SMCListView* ListView, SPaging* pPaging)
	{
		__super::Init(ListView);
		m_pSPaging = pPaging;
		m_pSPaging->GetEventSet()->subscribeEvent(EVT_PAGIN_PAGECHANGED, Subscriber(&SMCListViewPaging<T, M>::OnPageChanged, this));
	}
	bool OnPageChanged(EventArgs *pEvt)
	{
		EventPagingChanged* pPageEvent = sobj_cast<EventPagingChanged>(pEvt);
		UpdataListShow(pPageEvent->nCurSel);
		return true;
	}

	//设置一页展示多少
	void SetListCount(int Count)
	{
		m_ListCount = Count;
	}

protected:
	void UpdataListShow(int Sel)
	{
		__super::Clear();
		int PageIndex = Sel*m_ListCount;
		std::list<T>::iterator it = m_Data.begin();
		for (int n = 0; it != m_Data.end(); it++, n++)
		{
			if (n > PageIndex + m_ListCount-1)
				break;
			if (n >= PageIndex)
			{
				__super::push_back((*it));
			}
		}
		__super::UpdateView();
	}

private:
	SPaging *m_pSPaging;
	std::list<T> m_Data;//全部数据
	int m_ListCount;

};