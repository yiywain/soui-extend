#pragma once
#include <core\Swnd.h>

namespace SOUI
{
	class SFilletText :
		public SWindow
	{
		SOUI_CLASS_NAME(SLegend, L"FilletText")
	public:
		SFilletText();
		virtual ~SFilletText();

		void SetColor(COLORREF Color);
	protected:
		void OnPaint(IRenderTarget *pRT);

	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)
			SOUI_MSG_MAP_END()

			SOUI_ATTRS_BEGIN()
			ATTR_COLOR(L"colorFillet", m_ColorFillet, FALSE)
			ATTR_INT(L"fillet", m_Fillet, FALSE)
			SOUI_ATTRS_END()
	protected:
		int m_Fillet;
		COLORREF m_ColorFillet;
	};

}