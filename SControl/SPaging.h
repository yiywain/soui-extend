#pragma once
#include <core\Swnd.h>

#define EVT_PAGIN_BEGIN	(EVT_EXTERNAL_BEGIN + 1100)
#define EVT_PAGIN_PAGECHANGED		(EVT_PAGIN_BEGIN + 0)

namespace SOUI
{

	class EventPagingChanged : public TplEventArgs<EventPagingChanged>
	{
		SOUI_CLASS_NAME(EventPagingChanged, L"on_paging_changed")
	public:
		EventPagingChanged(SWindow *pSender) :TplEventArgs<EventPagingChanged>(pSender)
		{

		}
		enum{ EventID = EVT_PAGIN_PAGECHANGED };
		int		  nCurSel;
	};



	class SPaging :
		public SWindow
	{
		SOUI_CLASS_NAME(SLegend, L"Paging")
	public:
		SPaging();
		~SPaging();

		void SetCurSel(int Number);
		int GetCurSel();

		void SetPageCount(int Number);
		int GetPageCount();

		//清空编辑框文本
		void ClearEditText();
	protected:
		bool OnBtnBegin(EventArgs *pEvt);
		bool OnBtnLast(EventArgs *pEvt);
		bool OnBtnNext(EventArgs *pEvt);
		bool OnBtnEnd(EventArgs *pEvt);
		bool OnBtnSel(EventArgs *pEvt);

		bool OnBtnGo(EventArgs *pEvt);
	private:
		struct AttributeType;

	protected:
		/**
		* GetDesiredSize
		* @brief    当没有指定窗口大小时，通过如皮肤计算窗口的期望大小
		* @param    LPRECT pRcContainer --  容器位置
		* @return   CSize
		*
		* Describe
		*/
		virtual BOOL CreateChildren(pugi::xml_node xmlNode);
		virtual CSize GetDesiredSize(LPCRECT pRcContainer);
		int OnCreate(LPCREATESTRUCT lpCreateStruct);

		void SetSel(int Sel);
		SButton* CreateBtn(SStringW Text, int Data = 0);

		SOUI_ATTRS_BEGIN()
			//		ATTR_STRINGW(L"btnSkin", m_StrBtnSkin, FALSE)
			//		ATTR_STRINGW(L"selBtnSkin", m_StrSelBtnSkin, FALSE)

			ATTR_INT(L"pageNumber", m_ShowBtnNumber, FALSE)
			ATTR_BOOL(L"pageJumps", m_PageJumps, FALSE)
			SOUI_ATTRS_END()

			SOUI_MSG_MAP_BEGIN()
			//	MSG_WM_PAINT_EX(OnPaint)
			MSG_WM_CREATE(OnCreate)
			SOUI_MSG_MAP_END()
	private:
		void PageNumberChange();

		void UpdataBtn(void);

		void TouchEvent(void);

		//按钮状态更新
		void BtnStateUpdata();
	protected:
		SStringW m_StrBtnSkin;   /**< 按钮图片资源 */
		SStringW m_StrSelBtnSkin;   /**< 选中的按钮图片资源 */

		int m_ShowBtnNumber;    /**< 显示页数量 */

		int m_CurSel;//当前选中
		int m_PageCount;//页数量
		bool m_PageJumps;//是否启用页面跳转控件

		SButton *m_pBtnBeginPage;
		SButton *m_pBtnLastPage;
		SButton *m_pBtnNextPage;
		SButton *m_pBtnEndPage;
		SArray<SButton*> m_pPages;
		SButton *m_pBtnGo;
		SEdit *m_pEdit;
		SStatic *m_pText[2];


		SStringW m_BtnTemplate;
		SStringW m_EditTemplate;
		SStringW m_TxtTemplate;

	};

}