#include "stdafx.h"
#include "SFilletText.h"

namespace SOUI
{
	SFilletText::SFilletText() :m_Fillet(2), m_ColorFillet(RGBA(255, 0, 0, 255))
	{
	}


	SFilletText::~SFilletText()
	{
	}

	void SFilletText::SetColor(COLORREF Color)
	{
		m_ColorFillet = Color;
	}


	void SFilletText::OnPaint(IRenderTarget *pRT)
	{
		SPainter painter;
		BeforePaint(pRT, painter);
		CRect ClientRect = GetWindowRect();

		POINT po;
		po.x = po.y = m_Fillet;
		pRT->FillSolidRoundRect(ClientRect, po, m_ColorFillet);

		AfterPaint(pRT, painter);
		SWindow::OnPaint(pRT);


		//	CAutoRefPtr<SOUI::IBrush> pBrush;
		//	CAutoRefPtr<SOUI::IBrush> poldBrush;

		//poldBrush = (IBrush*)pRT->GetCurrentObject(OT_BRUSH);
		//pRT->CreateSolidColorBrush(m_ColorFillet, &pBrush);/*GetStyle().m_crBg*/
		//pRT->SelectObject(pBrush);
		//POINT po;
		//po.x = po.y = m_Fillet;
		//pRT->FillSolidRoundRect(ClientRect, po, m_ColorFillet);
		////Draw
		////pRT->FillRoundRect(ClientRect, po);

		////pRT->SelectObject(poldBrush);
		//AfterPaint(pRT, painter);
		//SWindow::OnPaint(pRT);
	}
}