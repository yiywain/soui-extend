#include "stdafx.h"
#include "SPaging.h"


SPaging::SPaging() :
m_StrSkin(L"_skin.sys.header"),
m_StrSelSkin(L"_skin.sys.header"), 
m_EditStrSkin(L""),
m_PageCount(900),
m_ShowBtnNumber(7),
m_CurSel(0),
m_PageJumps(true)
{
	m_evtSet.addEvent(EVENTID(EventPagingChanged));
	m_EditSize.cx = 50;
	m_EditSize.cy = 0;
}


SPaging::~SPaging()
{

}

int SPaging::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CRect rcClient = GetWindowRect();
	m_pBtnBeginPage = CreateBtn(L"首页");
	m_pBtnBeginPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnBegin, this));
	m_pBtnLastPage = CreateBtn(L"上一页");
	m_pBtnLastPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnLast, this));
	for (int i = 0; i < m_ShowBtnNumber; i++)
	{
		SButton *p = CreateBtn(SStringW().Format(L"%d", i + 1), i);
		p->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnSel, this));
		m_pPages.Add(p);
	}
	m_pBtnNextPage = CreateBtn(L"下一页");
	m_pBtnNextPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnNext, this));
	m_pBtnEndPage=CreateBtn(L"末页");
	m_pBtnEndPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnEnd, this));


	if (m_PageJumps)
	{
		CreateChildren(L"<text pos=\"[10, |0\" offset=\"0, -0.5\">跳至</text>");

		SStringW H;
		int W = m_EditSize.cx == 0 ? -1 : m_EditSize.cx;
		H.Format(L"%s", (m_EditSize.cy == 0 ? L"full" : SStringW().Format(L"%d",m_EditSize.cy)));
		if (m_EditStrSkin.GetLength()>0)
			m_pEdit = (SEdit*)CreateChildren(SStringW().Format(L"<edit pos=\"[10, |0\" size=\"%d,%s\"  offset=\"0,-0.5\" inset=\"1,1,1,1\" skin=\"%s\"  number=\"1\"/>", W, H, m_EditStrSkin));
		else
			m_pEdit = (SEdit*)CreateChildren(SStringW().Format(L"<edit pos=\"[10, |0\" size=\"%d,%s\" offset=\"0,-0.5\" inset=\"1,1,1,1\" margin=\"1,1,1,1\" number=\"1\"/>", W, H));


		CreateChildren(L"<text pos=\"[10, |0\" offset=\"0, -0.5\">页</text>");
		SStringW Str = SStringW().Format(L"<button pos=\"[10,0\" size=\"-1,full\" padding=\"10,0,10,0\" Cursor=\"hand\" drawFocusRect=\"0\" skin=\"%s\">GO</button>", m_StrSkin);
		m_pBtnGo = (SButton*)CreateChildren(Str);
		m_pBtnGo->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnGo, this));
	}

	//test
	UpdataBtn();
	return __super::OnCreate(lpCreateStruct);
}

//ID 存储了索引与Text不同
SButton* SPaging::CreateBtn(SStringW Text, int Data)
{
	return (SButton*)CreateChildren(SStringW().Format(
		L"<button pos=\"[12,0\" data=\"%d\" size=\"-1,full\" padding=\"10,0,10,0\" text=\"%s\" display=\"0\" Cursor=\"hand\" skin=\"%s\" drawFocusRect=\"0\"/>",
		Data,Text, m_StrSkin));
}



CSize SPaging::GetDesiredSize(LPCRECT pRcContainer)
{
	CSize Size;
	Size.cx = pRcContainer->right - pRcContainer->left;
	Size.cy = pRcContainer->bottom - pRcContainer->top;
	return Size;
}

void SPaging::SetCurSel(int Number)
{
	SetSel(Number);
}

int SPaging::GetCurSel()
{
	return m_CurSel;
}

void SPaging::SetSel(int Sel)
{


}

bool SPaging::OnBtnBegin(EventArgs *pEvt)
{
	m_CurSel = 0;
	EventPagingChanged evt(this);
	evt.nCurSel = m_CurSel;
	FireEvent(evt);

	UpdataBtn();
	return true;
}

bool SPaging::OnBtnLast(EventArgs *pEvt)
{
	if(m_CurSel>0)
		m_CurSel--;

	EventPagingChanged evt(this);
	evt.nCurSel = m_CurSel;
	FireEvent(evt);

	UpdataBtn();
	return true;
}

bool SPaging::OnBtnNext(EventArgs *pEvt)
{

	if (m_CurSel<m_PageCount-1)
		m_CurSel++;

	EventPagingChanged evt(this);
	evt.nCurSel = m_CurSel;
	FireEvent(evt);

	UpdataBtn();
	return true;
}

bool SPaging::OnBtnEnd(EventArgs *pEvt)
{
	if (m_CurSel<m_PageCount)
		m_CurSel = m_PageCount-1;

	EventPagingChanged evt(this);
	evt.nCurSel = m_CurSel;
	FireEvent(evt);

	UpdataBtn();
	return true;
}

bool SPaging::OnBtnSel(EventArgs *pEvt)
{
	SWindow* pImgBtn = sobj_cast<SWindow>(pEvt->sender);
	if (pImgBtn)
	{
		m_CurSel=pImgBtn->GetUserData();

		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
	}
	return true;
}

void SPaging::SetPageCount(int Number)
{
	m_PageCount = Number <= 0 ? 1 : Number;
	PageNumberChange();
}
int SPaging::GetPageCount()
{
	return m_PageCount;
}

void SPaging::PageNumberChange()
{
	for (int i = 0; i < m_pPages.GetCount(); i++)
	{
		if (m_PageCount >= m_ShowBtnNumber)
			m_pPages[i]->SetVisible(i<m_ShowBtnNumber);
		else
			m_pPages[i]->SetVisible(i<m_PageCount);
	}
	BtnStateUpdata(); 
}

void SPaging::UpdataBtn(void)
{
	int Neutral = m_pPages.GetCount()/2;
	for (int i = 0; i < m_pPages.GetCount(); i++)
	{
		//m_pPages[i]->SetVisible(i<m_ShowBtnNumber);
		bool flag = m_PageCount >= m_ShowBtnNumber;
		if (flag)
			m_pPages[i]->SetVisible(i<m_ShowBtnNumber);
		else
			m_pPages[i]->SetVisible(i<m_PageCount);


		if (flag ? i<m_ShowBtnNumber : i<m_PageCount)
		{
			int Number;
			if (m_PageCount - Neutral>m_CurSel)
			{
				if (m_CurSel > Neutral)
					Number = m_CurSel - Neutral + i + 1;
				else
					Number=i + 1;
			}
			else
			{
				if (flag)
					Number = m_PageCount - m_ShowBtnNumber + i+1;
				else
					Number =i + 1;
			}
			m_pPages[i]->SetWindowText(SStringW().Format(L"%d", Number));
			m_pPages[i]->SetUserData(Number-1);

			if (Number == m_CurSel+1)
				m_pPages[i]->SetAttribute(L"skin", m_StrSelSkin);
			else
				m_pPages[i]->SetAttribute(L"skin", m_StrSkin);
		}
	}
	BtnStateUpdata();

}

bool SPaging::OnBtnGo(EventArgs *pEvt)
{
	int Number=_wtoi(m_pEdit->GetWindowText())-1;
	if (Number >= 0 && Number<m_PageCount)
	{
		m_CurSel = Number;
		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
	}
	return true;
}

void SPaging::TouchEvent(void)
{
	EventPagingChanged evt(this);
	evt.nCurSel = m_CurSel;
	FireEvent(evt);
}

void SPaging::BtnStateUpdata()
{
	//按钮禁用
	if (m_CurSel == m_PageCount - 1)
	{
		m_pBtnNextPage->EnableWindow(FALSE, TRUE);
		m_pBtnEndPage->EnableWindow(FALSE, TRUE);
	}
	else
	{
		m_pBtnNextPage->EnableWindow(TRUE, TRUE);
		m_pBtnEndPage->EnableWindow(TRUE, TRUE);
	}
	if (m_CurSel == 0)
	{
		m_pBtnBeginPage->EnableWindow(FALSE, TRUE);
		m_pBtnLastPage->EnableWindow(FALSE, TRUE);
	}
	else
	{
		m_pBtnBeginPage->EnableWindow(TRUE, TRUE);
		m_pBtnLastPage->EnableWindow(TRUE, TRUE);
	}

}