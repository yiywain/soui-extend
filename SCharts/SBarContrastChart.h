/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file       SBarContrastChart.h
* @brief
* @version    v1.0
* @author     yiywain(逸远)
* @date       2020/01/07
* gitee  https://gitee.com/yiywain/soui-extend.git
* Describe    上下柱状图
*/

#pragma once
#include <list>

namespace SOUI
{
	class SBarContrastChart:public SWindow
	{
		SOUI_CLASS_NAME(SBarContrastChart, L"BarContrastChart")
	public:

		struct Bar_s
		{
			float Value;
			COLORREF ColorBegin;
			COLORREF ColorEnd;
			float GradualHeight;//渐变高度 可为百分比
			bool GraduaIsMaxRate;//百分比时是否为最大的比例
			Bar_s()
			{
				GradualHeight = 0.2;
				GraduaIsMaxRate = true;
				Value = 0;
			}
			Bar_s(float Value, COLORREF ColorBegin, COLORREF ColorEnd, float GradualHeight, bool GraduaIsMaxRate)
			{
				this->Value = Value;
				this->ColorBegin = ColorBegin;
				this->ColorEnd = ColorEnd;
				this->GradualHeight = GradualHeight;
				this->GraduaIsMaxRate = GraduaIsMaxRate;
			}
		};
		struct BarAttribute{
			float MaxValue; //最大值
			float MinValue; //最小值
			SStringW Ttitle;//标题
			int Decimal;//小数点位数
			SStringW Unit;//单位
			Bar_s Bar[2];
			BarAttribute()
			{
			}
			BarAttribute(SStringW &Str, int Decimal, wchar_t *Unit, float Min, float Max, Bar_s &Bar1,Bar_s &Bar2)
			{
				Ttitle = Str;
				this->Decimal = Decimal;
				this->Unit = Unit;
				MinValue = Min;
				MaxValue = Max;
				Bar[0] = Bar1;
				Bar[1] = Bar2;		
			}
		};

		SBarContrastChart();
		~SBarContrastChart();

		void AddBar(BarAttribute &Bar);

		void SetV1Data(int index,float Value,bool IsUpdataView = false);
		void SetV2Data(int index, float Value, bool IsUpdataView = false);
		void SetData(int index, float Value1,float Value2,bool IsUpdataView = false);

		void Clear(void);

		bool Remove(int index);

		void ShowValue(bool Show);

		void SetBarInterval(float Interval);

		void SetBarWidth(float Width);

		void UpdateWindow()
		{
			InvalidateRect(&GetClientRect());
		}
	protected:


		void OnPaint(IRenderTarget *pRT);

	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)
		SOUI_MSG_MAP_END()

		SOUI_ATTRS_BEGIN()
			ATTR_RECT(L"CoordMargin", m_CoordMargin, TRUE)
			ATTR_COLOR(L"BaseLineColor", m_BaseLineColor,TRUE)
			ATTR_INT(L"BaseLineWidth", m_BaseLineWidth, TRUE)

			ATTR_COLOR(L"BarColor", m_BarColor, TRUE)
			ATTR_FLOAT(L"BarWidth", m_BarWidth, TRUE)
			ATTR_BOOL(L"ShowValue", m_ShowValue, TRUE)

			ATTR_FONT(L"BarTextFont", m_BarTextFont, TRUE)
			ATTR_COLOR(L"BarTextColor", m_BarTextColor, TRUE)
		SOUI_ATTRS_END()
	protected:
		CRect m_CoordMargin;//内边距

		COLORREF m_BaseLineColor;//基线颜色
		int m_BaseLineWidth;//基线宽度

		float m_BarWidth;//单个柱的宽度 支持百分比
		COLORREF m_BarColor;//柱的默认颜色
		bool m_ShowValue;//是否显示数值

		SDpiAwareFont m_BarTextFont;//柱数值文本字体
		COLORREF m_BarTextColor;//柱数值文本颜色
		float m_BarGradientHeight;//柱默认渐变高度

		//Other
		CRect m_DataRect;
	private:
		std::list<BarAttribute> m_Data;
	};

}