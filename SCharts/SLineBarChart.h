//***********************************************
// Copyright 2019-2029
// All rights reserved
// 
// 文件名：	SLineBarChart
// 作者：	YiJia
// 时间：	2021/03/12 16:21:19
// 描述：	柱图、折线混合图
// 修改记录：
// 修改时间：
//***********************************************

#pragma once
#include "SCoordAxis.h"
#include "SBarChart.h"

namespace SOUI
{
	class SLineBarChart :public SBarChart
	{
		SOUI_CLASS_NAME(SLineBarChart, L"LineBarChart")
	public:
		SLineBarChart();
		~SLineBarChart();

		void AddLineData(int id, float Value);

		void AddLineData(int id, float Value, COLORREF Color);

		void SetLineData(int id, SArray<BarDataType> &Data);

		SArray<BarDataType> &GetLineData(int id);

		void ClearLineData(void);

		bool RemoveLineData(int id);

		//设置是否按百分比样式
		void SetPercent(bool IsPercent=false);

		void ShowValue(bool Show);

	protected:
		void OnPaint(IRenderTarget *pRT);

	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)
		SOUI_MSG_MAP_END()

		SOUI_ATTRS_BEGIN()
			ATTR_INT(L"LineWidth", m_LineWidth, TRUE)
		SOUI_ATTRS_END()
	protected:
		COLORREF m_LineColor;//柱的默认颜色
		bool m_ShowLineValue;//是否显示折线数值
		SDpiAwareFont m_LineTextFont;//
		COLORREF m_LineTextColor;//
		int m_LineDecimal;//折线显示的小数位
		int m_LineWidth;//折线宽度
	private:
		typedef SMap<int, SArray<BarDataType>> CoordDataType;
		CoordDataType m_LineData;

		bool hasRightAxis;
		bool m_IsPercent;
	};

}