/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file       SBarGradualChart.h
* @brief
* @version    v1.0
* @author     yiywain(逸远)
* @date       2019/12/19
* gitee		  https://gitee.com/yiywain/soui-extend.git
* Describe    支持渐变色的柱状图(使用GDI+)
*/

#pragma once
#include "SCoordAxis.h"
#include <GdiPlus.h>

#define DEF_BAR_COLOR1 RGBA(137, 139, 255, 255)
#define DEF_BAR_COLOR2 RGBA(74, 109, 255, 255)

namespace SOUI
{
	struct CircularBeadPo
	{
		bool LeftTop;
		bool RightTop;
		bool RightBottom;
		bool LeftBottom;
		CircularBeadPo()
		{
			LeftTop = RightTop = RightBottom = LeftBottom = false;
		}
		CircularBeadPo(bool c1, bool c2, bool c3, bool c4)
		{
			LeftTop = c1;
			RightTop = c2;
			RightBottom = c3;
			LeftBottom = c4;
		}
		bool IsEmpty()
		{
			return (!LeftTop) && (!RightTop) && (!RightBottom) && (!LeftBottom);
		}
	};
	

	class CGraphicsRoundRectPath : public Gdiplus::GraphicsPath
	{

	public:
		CGraphicsRoundRectPath();
		CGraphicsRoundRectPath(INT x, INT y, INT width, INT height, INT cornerX, INT cornerY, CircularBeadPo FilletBead = CircularBeadPo());

	public:
		void AddRoundRect(INT x, INT y, INT width, INT height, INT cornerX, INT cornerY, CircularBeadPo FilletBead = CircularBeadPo());
	};

	class SBarGradualChart:
		public SCoordAxis
	{
		SOUI_CLASS_NAME(SChart_bar, L"BarGradualChart")
	public:

		struct BarDataType{
			float Value;
			COLORREF BarColor1;//渐变色1
			COLORREF BarColor2;//渐变色2
			float Angle;//线性渐变角度
			bool IsShowValue;//是否显示值
			BarDataType &operator=(const BarDataType& Data)
			{
				Value = Data.Value;
				BarColor1 = Data.BarColor1;
				BarColor2 = Data.BarColor2;
				Angle = Data.Angle;
				IsShowValue = Data.IsShowValue;
				return *this;
			}
			BarDataType()
			{
				Value = 0;
				BarColor1 = DEF_BAR_COLOR1;
				BarColor2 = DEF_BAR_COLOR2;
				Angle = 0;
				IsShowValue = true;
			}
		};

		SBarGradualChart();
		~SBarGradualChart();

		void AddData(int id, float Value, float Angle=90,bool IsShowValue = true);

		void AddData(int id, float Value, COLORREF BarColor1, COLORREF BarColor2, float Angle, bool IsShowValue = true);

		void SetData(int id, SArray<BarDataType> &Data);

		SArray<BarDataType> &GetData(int id);

		void Clear(void);

		bool Remove(int id);

		//设置显示单位
		void SetUnit(SStringT Unit);

		void ShowValue(bool Show);

		void SetBarInterval(float Interval);

		void SetBarWidth(float Width);

		void UpdateWindow()
		{
			InvalidateRect(&GetClientRect());
		}
	protected:

		virtual void GetMaxMin(float &Max, float &Min);

		void OnPaint(IRenderTarget *pRT);

		HRESULT OnAttrtFilletPoint(const SStringW& strValue, BOOL bLoading);
	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)

			SOUI_MSG_MAP_END()

			SOUI_ATTRS_BEGIN()
				/*为了兼容之前的纯色柱状图参数*/
				ATTR_COLOR(L"BarColor", m_BarColor1, FALSE)
				ATTR_COLOR(L"BarColor", m_BarColor2, FALSE)
				/**/
				ATTR_COLOR(L"BarColor1", m_BarColor1, FALSE)
				ATTR_COLOR(L"BarColor2", m_BarColor2, FALSE)
				ATTR_FLOAT(L"BarWidth", m_BarWidth, FALSE)
				ATTR_FLOAT(L"BarInterval", m_BarInterval, FALSE)
				ATTR_BOOL(L"ShowValue", m_ShowValue, FALSE)
				ATTR_STRINGT(L"Unit", m_Unit, FALSE)

				ATTR_FONT(L"BarTextFont", m_BarTextFont, FALSE)
				ATTR_COLOR(L"BarTextColor", m_BarTextColor, FALSE)
				ATTR_FLOAT(L"Fillet",m_Fillet,FALSE)
				ATTR_CUSTOM(L"FilletPoint", OnAttrtFilletPoint)

				ATTR_POINT(L"GradualRange", m_GradualRange, FALSE)//0,0 1,1 1,0 0,1
			SOUI_ATTRS_END()
	protected:
		float m_BarWidth;//单个柱的宽度 支持百分比
		float m_BarInterval;//柱间隔 支持百分比
		COLORREF m_BarColor1;//柱的默认颜色1
		COLORREF m_BarColor2;//柱的默认颜色2
		bool m_ShowValue;//是否显示数值 总开关
		SStringT m_Unit;//显示数值的单位 不显示则无效

		SDpiAwareFont m_BarTextFont;//
		COLORREF m_BarTextColor;//
		float m_Fillet;//圆角大小0-0.9或 1-n n<m_BarWidth/2
		POINT m_GradualRange;//渐变范围0为图表渐变 1为图表 (例如:0,0)

		CircularBeadPo m_FilletPoint;//圆角位置
	private:
		typedef SMap<int, SArray<BarDataType>> CoordDataType;
		CoordDataType m_Data;
	};

}