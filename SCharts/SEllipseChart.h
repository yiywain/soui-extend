/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file       SEllipseChart.h
* @brief
* @version    v1.0
* @author     yiywain(��Զ)
* @date       2020/1/7
* gitee  https://gitee.com/yiywain/soui-extend.git
* Describe    ��״ͼ
*/

#pragma once
#include "SCoordAxis.h"
namespace SOUI
{
	class SEllipseChart :
		public SCoordAxis
	{
		SOUI_CLASS_NAME(SEllipseChart, L"EllipseChart")
	public:

		struct EllipseDataType{
			float Value;
			float Width;//��Բ�Ŀ���
			COLORREF BeginColor;
			COLORREF EndColor;
			EllipseDataType(float Value, float Width,COLORREF BeginColor, COLORREF EndColor)
			{
				this->Value = Value;
				this->Width = Width;
				this->BeginColor = BeginColor;
				this->EndColor = EndColor;
			}
			EllipseDataType()
			{
				Width=Value = 0;
			}
		};

		SEllipseChart();
		~SEllipseChart();

		void AddData(int id, float Value,float Width);

		void AddData(int id, EllipseDataType &Data);

		void SetData(int id, EllipseDataType &Data);

		EllipseDataType &GetData(int id);

		void Clear(void);

		bool Remove(int id);

		void UpdateWindow()
		{
			InvalidateRect(&GetClientRect());
		}
	protected:

		virtual void GetMaxMin(float &Max, float &Min);

		void OnPaint(IRenderTarget *pRT);

	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)

			SOUI_MSG_MAP_END()

			SOUI_ATTRS_BEGIN()
			ATTR_COLOR(L"EllipseColor", m_DefEllipseBeginColor, TRUE)

			SOUI_ATTRS_END()
	protected:
		COLORREF m_DefEllipseBeginColor;//Ĭ����ʼ��ɫ

	private:
		typedef SMap<int,EllipseDataType> CoordDataType;
		CoordDataType m_Data;
	};

}