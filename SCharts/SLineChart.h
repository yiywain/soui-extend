/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file       SLineChart.h
* @brief
* @version    v1.0
* @author     yiywain(逸远)
* @date       2020/6/8
* gitee		  https://gitee.com/yiywain/soui-extend.git
* Describe    线性渐变曲线图
*/

#pragma once
#include "SCoordAxis.h"

//默认张力
#define DEF_TENSION 0.5f
//默认填充色为透明
#define LUCENCY_COLOR (RGBA(255,255,255,0))

namespace SOUI
{

	class SLineChart :
		public SCoordAxis
	{
		SOUI_CLASS_NAME(SChart_bar, L"LineChart")
	public:
		struct LineTag
		{
			SStringT Name;
			SArray<float> Values;
			SArray<SStringT> Texts;
			COLORREF LineColor;//线颜色
			COLORREF FillColor1;//填充颜色1
			COLORREF FillColor2;//填充颜色2
			float Angle;//线性渐变角度
			float Tension;//弯曲强度 范围(0~1.f) 默认0.5 曲线图有效 0为直线图
			LineTag()
			{
				Name = L"";
				LineColor = RGBA(0, 0, 0, 255);
				Tension = DEF_TENSION;
			}
			LineTag(SStringT Name, SArray<float> &Values, SArray<SStringT> &Texts, COLORREF LineColor, COLORREF FillColor1 = LUCENCY_COLOR, COLORREF FillColor2 = LUCENCY_COLOR, float Angle=0, float Tension = DEF_TENSION)
			{
				this->Name = Name;
				this->Texts = Texts;
				this->Values = Values;
				this->LineColor = LineColor;
				this->FillColor1 = FillColor1;
				this->FillColor2 = FillColor2;
				this->Angle = Angle;
				this->Tension = Tension ;
			}
		};

		SLineChart();
		~SLineChart();

		//创建一条线
		void CreateLine(int id, SStringT Name, COLORREF LineColor, COLORREF FillColor1, COLORREF FillColor2, float Angle = 0, float Tension = DEF_TENSION);
		//创建一条线
		void CreateLine(int id, SStringT Name, COLORREF LineColor,float Tension = DEF_TENSION);

		//向线添加数据
		void AddData(int id, float Value,SStringT Text=L"");

		//清空所有数据
		void Clear();

		//修改线的数据
		void SetLineData(int id, LineTag &LineData);

		//获取线的数据
		LineTag &GetLineData(int id);

		void SetLineValue(int id, int index, float Value);

		void SetLineText(int id, int index, SStringT Text);

		//设置线宽
		void SetLineWidth(int Width);

		//设置填充透明通道
		void SetFill(int IsFill);

		//设置数据到哪里结束 支持百分比
		void SetDataEnd(float End);

		//设置是否为曲线图
		void SetCurveChart(bool Curve);

		//设置线的弯曲强度
		void SetLineTension(int id, float Tension);

		void PopTopPushBack(int id, float Value, SStringT Text=L"");

		void UpdateWindow()
		{
			InvalidateRect(&GetClientRect());
		}
	protected:
		virtual void GetMaxMin(float &Max, float &Min);

		void OnPaint(IRenderTarget *pRT);
		void OnMouseMove(UINT nFlags, CPoint point);
		void OnMouseLeave();
	protected:
		SOUI_MSG_MAP_BEGIN()
			MSG_WM_PAINT_EX(OnPaint)
			MSG_WM_MOUSEMOVE(OnMouseMove)
			MSG_WM_MOUSELEAVE(OnMouseLeave)
			SOUI_MSG_MAP_END()

			SOUI_ATTRS_BEGIN()
			ATTR_COLOR(L"LineColor", m_LineColor, TRUE)
			ATTR_INT(L"LineWidth", m_LineWidth, TRUE)
			ATTR_BOOL(L"IsFill", m_IsFill, TRUE)
			ATTR_FLOAT(L"DataEnd", m_DataEnd, TRUE)
			ATTR_BOOL(L"IsCurve", m_IsCurve, TRUE)
			ATTR_INT(L"DataPointSize", m_DataPointSize, TRUE)
			ATTR_COLOR(L"DataPointCentreColor", m_DataPointCentreColor, TRUE)

			ATTR_BOOL(L"CursorTip", m_CursorTip, TRUE)
			ATTR_FONT(L"CursorTipFont", m_CursorTipFont, TRUE)
			ATTR_COLOR(L"CursorTipTextColor", m_CursorTipTextColor, TRUE)
			ATTR_COLOR(L"CursorTipColorBkgnd", m_CursorTipColorBkgnd, TRUE)

			ATTR_COLOR(L"CursorLineColor", m_CursorLineColor, TRUE)
			ATTR_INT(L"CursorLineWidth", m_CursorLineWidth, TRUE)
			SOUI_ATTRS_END()
	protected:
		COLORREF m_LineColor;
		int m_LineWidth;
		bool m_IsFill;//填充区域透明通道 0为不填充0-255
		float m_DataEnd;//数据结束位置 0为不设置 支持百分比
		bool m_IsCurve;//是否是曲线 曲线暂不支持区域填充

		INT m_DataPointSize;//数据点大小
		COLORREF m_DataPointCentreColor;//数据点颜色

		BOOL m_CursorTip; //是否响应鼠标
		SDpiAwareFont m_CursorTipFont;//鼠标提示字体
		COLORREF m_CursorTipTextColor;//鼠标提示字体颜色
		COLORREF m_CursorTipColorBkgnd;
		COLORREF m_CursorLineColor;
		INT m_CursorLineWidth;

		typedef SMap<int, LineTag> CoordLineData;
		CoordLineData m_Data;
	private:
		bool m_CursorInCoord;
		CPoint m_CursorPoint;
	};

}