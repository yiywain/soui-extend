#include "stdafx.h"
#include "SLegend.h"
#include <GdiPlus.h>
#pragma comment(lib,"gdiplus")
//#define LEGEND_WIDTH 20

namespace SOUI
{
	SLegendCheckBox::SLegendCheckBox() :
		m_Type(LegendCheckType_Ellipse),
		m_Color(RGBA(255, 0, 0, 255)),
		m_LegendSize(17)
	{


	}
	SLegendCheckBox::~SLegendCheckBox()
	{


	}

	void SLegendCheckBox::OnPaint(IRenderTarget *pRT)
	{

		//	__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);
		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);

		CRect ClientRect = GetWindowRect();
		CRect MarkRect;
		MarkRect.left = ClientRect.left;
		MarkRect.right = MarkRect.left + m_LegendSize;

		MarkRect.top = ClientRect.top;
		MarkRect.bottom = MarkRect.top + m_LegendSize;
		MarkRect.OffsetRect(0, ClientRect.Height() / 2 - MarkRect.Height() / 2);

		CAutoRefPtr<SOUI::IBrush> pBrush;
		CAutoRefPtr<SOUI::IBrush> poldBrush;
		CAutoRefPtr<SOUI::IPen> pPen;
		CAutoRefPtr<IRenderObj> poldPen = pRT->GetCurrentObject(OT_PEN);
		poldBrush = (IBrush*)pRT->GetCurrentObject(OT_BRUSH);
		pRT->CreateSolidColorBrush(m_Color, &pBrush);
		pRT->CreatePen(PS_SOLID, m_Color, 0, &pPen);
		pRT->SelectObject(pPen);
		pRT->SelectObject(pBrush);
		switch (m_Type)
		{
		case SOUI::LegendCheckType_Ellipse:
		{
			pRT->FillEllipse(MarkRect);
			break;
		}
		case SOUI::LegendCheckType_Triangle:
		{
			POINT Pos[3];
			Pos[0].x = MarkRect.left + MarkRect.Width() / 2;
			Pos[0].y = MarkRect.top;
			Pos[1] = MarkRect.BottomRight();
			Pos[2].y = MarkRect.bottom;
			Pos[2].x = MarkRect.left;

			//Skia支持的情况
			CAutoRefPtr<SOUI::IPath> pPath;
			if (GETRENDERFACTORY->CreatePath(&pPath))
			{
				pPath->addPoly(Pos, 3, false);
				pRT->FillPath(pPath);
			}
			else
			{
				HDC hDC = pRT->GetDC();
				Gdiplus::Graphics graph(hDC);
				Gdiplus::SolidBrush Brush(Gdiplus::Color(GetAValue(m_Color), GetRValue(m_Color), GetGValue(m_Color), GetBValue(m_Color)));
				Gdiplus::Point Po[3];
				for (int i = 0; i < 3; i++)
				{
					Po[i].X = Pos[i].x;
					Po[i].Y = Pos[i].y;
				}
				graph.FillPolygon(&Brush, Po, 3);
				pRT->ReleaseDC(hDC);
			}
			break;
		}
		case SOUI::LegendCheckType_Rectangle:
		{
			pRT->FillRectangle(MarkRect);
			break;
		}
		case SOUI::LegendCheckType_RoundedRectangle:
		{
			POINT po;
			po.x = po.y = 2;
			pRT->FillRoundRect(MarkRect, po);
			break;
		}
		default:
			break;
		}
		pRT->SelectObject(poldBrush);
		pRT->SelectObject(poldPen);

		//	CRect TextRect;
		//	TextRect = ClientRect;
		//	TextRect.left = MarkRect.right;
		//	pRT->DrawText(m_strText.GetText(), m_strText.GetText().GetLength(), TextRect, DT_LEFT | DT_VCENTER);

		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		AfterPaint(pRT, painter);

		SWindow::OnPaint(pRT);
	}

	CSize SLegendCheckBox::GetDesiredSize(LPCRECT pRcContainer)
	{
		CSize Size = SWindow::GetDesiredSize(pRcContainer);
		Size.cx += m_LegendSize + CheckBoxSpacing;
		Size.cy = (std::max)((int)Size.cy, m_LegendSize);
		return Size;
	}

	void SLegendCheckBox::GetTextRect(LPRECT pRect)
	{
		GetClientRect(pRect);
		pRect->left += m_LegendSize + CheckBoxSpacing;
	}

	void SLegendCheckBox::SetBoxColor(COLORREF Color)
	{
		m_Color = Color;
	}

	COLORREF SLegendCheckBox::GetBoxColor(void)
	{
		return m_Color;
	}

	SLegend::SLegend() :
		m_LayoutType(LayoutTypeEnum_Fixation),
		m_xNumber(5),
		m_yNumber(5),
		m_xSpace(1),
		m_ySpace(1)
	{


	}


	SLegend::~SLegend()
	{


	}

	void SLegend::OnPaint(IRenderTarget *pRT)
	{
		//绘制轴线
		SPainter painter;
		BeforePaint(pRT, painter);

		AfterPaint(pRT, painter);
	}


	void SLegend::Clear(void)
	{
		for (Legend &t : m_Data)
		{
			if (t.pLegend)
			{
				RemoveChild(t.pLegend);
				t.pLegend->Release();
			}
		}
		m_Data.clear();
	}

	void SLegend::Add(Legend &Legend)
	{
		AddLegendChild(Legend);
		m_Data.push_back(Legend);
	}

	void SLegend::AddLegendChild(Legend &Legend)
	{
		if (m_LayoutType == LayoutTypeEnum_Self_Adaption)
			Legend.pLegend = (SLegendCheckBox*)CreateChildren(L"<legendCheck size=\"0,0\" columnWeight=\"1\" rowWeight=\"1\"/>");
		else
			Legend.pLegend = (SLegendCheckBox*)CreateChildren(L"<legendCheck />");

		Legend.pLegend->SetWindowText(Legend.Text);
		Legend.pLegend->SetBoxColor(Legend.Color);
		Legend.pLegend->SetAttribute(L"Align", L"left");
	}

	int SLegend::OnCreate(LPCREATESTRUCT lpCreateStruct)
	{
		if (m_LayoutType == LayoutTypeEnum_Self_Adaption)
		{
			SetAttribute(L"layout", L"gridLayout");
			SetAttribute(L"xInterval", m_xSpace);
			SetAttribute(L"yInterval", m_ySpace);
			SetAttribute(L"xGravity", L"fill");
			SetAttribute(L"yGravity", L"fill");
		}
		return 0;
	}


	void SLegend::UpdateWindow(void)
	{
		if (m_LayoutType == LayoutTypeEnum_Fixation)
			LayoutFixation();
		else if (m_LayoutType == LayoutTypeEnum_Self_Adaption)
		{
			SetAttribute(L"columnCount", SStringW().Format(L"%d", m_xNumber));
			SetAttribute(L"rowCount", SStringW().Format(L"%d", m_yNumber));
		}

		UpdateLayout();
	}

	void SLegend::Setxy(int x, int y, bool IsUpdate)
	{
		m_xNumber = x;
		m_yNumber = y;

		if (IsUpdate)
			UpdateWindow();
	}



	void SLegend::LayoutFixation()
	{
		int i = 0;
		for (Legend &t : m_Data)
		{
			if ((i%m_xNumber) != 0)
			{
				t.pLegend->SetAttribute(L"pos", SStringT().Format(L"[%d,{0", m_xSpace));
			}
			else
			{
				t.pLegend->SetAttribute(L"pos", SStringT().Format(L"0,[%d", i < m_xNumber ? 0 : m_ySpace));
			}
			i++;
		}

	}

	int SLegend::Size(void)
	{
		return m_Data.size();
	}

	Legend *SLegend::GetAt(int i)
	{
		if (i < m_Data.size())
			return &(m_Data[i]);
		return NULL;

	}

	//
	//void SLegend::SyncWindowToData(void)
	//{
	//	m_Data.clear();
	//	SLegendCheckBox *p = static_cast<SLegendCheckBox*>(GetWindow(GSW_FIRSTCHILD));
	//	SWindow *sw = __super::GetWindow(GSW_FIRSTCHILD);
	//	if (p)
	//	{
	//		do
	//		{
	//			Legend leg;
	//			leg.Color = p->GetBoxColor();
	//			leg.Text = p->GetWindowText();
	//			leg.pLegend = p;
	//			m_Data.push_back(leg);
	//			p = static_cast<SLegendCheckBox*>(p->GetWindow(GSW_NEXTSIBLING));
	//		} while (p);
	//	}
	//	//SyncWindowToData();
	//}

}