#include "stdafx.h"
#include "SLineChart.h"
#include <GdiPlus.h>
#pragma comment(lib,"gdiplus")

namespace SOUI
{
	enum{
		TIP_MARGIN = 5//留出一点边距好看一点
	};

	SLineChart::SLineChart() :
		m_LineColor(RGBA(255, 255, 255, 255)),
		m_LineWidth(1),
		m_IsFill(0),
		m_DataEnd(0),
		m_IsCurve(false),
		m_DataPointSize(0),
		m_DataPointCentreColor(RGBA(0, 0, 0, 0)),
		m_CursorTip(true),
		m_CursorInCoord(false),
		m_CursorTipTextColor(RGBA(0, 0, 0, 255)),
		m_CursorTipColorBkgnd(RGBA(128, 128, 128, 128)),
		m_CursorLineColor(RGBA(128, 128, 128, 128)),
		m_CursorLineWidth(1)
	{

	}


	SLineChart::~SLineChart()
	{

	}

	void SLineChart::CreateLine(int id, SStringT Name, COLORREF LineColor, COLORREF FillColor1, COLORREF FillColor2, float Angle, float Tension)
	{
		LineTag t;
		t.LineColor = LineColor;
		t.Name = Name;
		t.FillColor1 = FillColor1;
		t.FillColor2 = FillColor2;
		t.Angle = Angle;
		t.Tension = Tension;
		m_Data[id] = t;
	}

	void SLineChart::CreateLine(int id, SStringT Name, COLORREF LineColor, float Tension)
	{
		CreateLine(id, Name, LineColor, LUCENCY_COLOR, LUCENCY_COLOR, 0, Tension);
	}

	void SLineChart::AddData(int id, float Value, SStringT Text)
	{
		m_Data[id].Values.Add(Value);
		m_Data[id].Texts.Add(Text);
	}

	void  SLineChart::Clear()
	{
		m_Data.RemoveAll();
	}



	void SLineChart::SetLineData(int id, LineTag &LineData)
	{
		m_Data[id] = LineData;
	}


	void SLineChart::OnMouseMove(UINT nFlags, CPoint point)
	{
		if (m_CursorTip)
		{
			m_CursorPoint = point;
			InvalidateRect(&GetClientRect(), false);
		}
		return;
	}

	void SLineChart::OnMouseLeave()
	{
		if (m_CursorTip)
		{
			m_CursorPoint.x = m_CursorPoint.y = 0;
			InvalidateRect(&GetClientRect());
		}
	}



	void SLineChart::OnPaint(IRenderTarget *pRT)
	{
		__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);
		HDC hdc = pRT->GetDC();
		Gdiplus::Graphics gps(hdc);
		gps.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);

		CRect rcClient = GetClientRect();
		CRect CurrentDrawRect;
		CurrentDrawRect = m_DataRect;

		if (!m_PullOver&&m_AxisType == AXIS_LENGTHWAYS)
		{
			CurrentDrawRect.left += (LONG)m_XSignRatio;
			CurrentDrawRect.right -= (LONG)m_XSignRatio;
		}
		else if (!m_PullOver&&m_AxisType == AXIS_CROSSWISE)
		{
			CurrentDrawRect.top += (LONG)m_YSignRatio;
			CurrentDrawRect.bottom -= (LONG)m_YSignRatio;
		}

		//对于m_DataEnd的支持	
		float DataWidth;
		if (m_DataEnd&&m_AxisType == AXIS_LENGTHWAYS)
		{
			DataWidth = m_DataEnd > 1 ? m_XSignRatio*m_DataEnd : CurrentDrawRect.Width()*(float)m_DataEnd;
			CurrentDrawRect.right = CurrentDrawRect.left + (LONG)DataWidth;
		}
		else if (m_DataEnd&&m_AxisType == AXIS_CROSSWISE)
		{
			DataWidth = m_DataEnd > 1 ? m_YSignRatio*m_DataEnd : CurrentDrawRect.Height()*(float)m_DataEnd;
			CurrentDrawRect.top = CurrentDrawRect.bottom - (LONG)DataWidth;
		}

		SOUI::SPOSITION pos = m_Data.GetStartPosition();
		bool CountAligned = true;//数据是否是对齐的
		size_t AlignedCount = 0;//CountAligned为真 有效
		int m = 0;

		SArray<SArray<POINT>> Pointsx;//线上的点坐标

		//每条线的绘制
		while (pos)
		{
			SArray<POINT> Points;
			CoordLineData::CPair *p = m_Data.GetNext(pos);

			SArray<Gdiplus::PointF> pos;

			Gdiplus::GraphicsPath Path;
			if (m == 0)
				m = p->m_value.Values.GetCount();
			else if (m != p->m_value.Values.GetCount())
				CountAligned = false;

			AlignedCount = p->m_value.Values.GetCount() > AlignedCount ? p->m_value.Values.GetCount() : AlignedCount;

			size_t Count = p->m_value.Values.GetCount() - 1;
			Count = Count < m_Text.GetCount() ? m_Text.GetCount() - 1 : Count;
			for (size_t i = 0; i < p->m_value.Values.GetCount(); i++)
			{
				POINT po;
				if (m_AxisType == AXIS_LENGTHWAYS)
				{
					po.x = GetDataDrawRect().left + (m_PullOver ? 0 : m_XSignRatio) + CurrentDrawRect.Width() / (float)(Count)*i;
					po.y = GetDataDrawRect().bottom - (m_ValueRatio*(p->m_value.Values[i] - m_MinValue));
				}
				else if (m_AxisType == AXIS_CROSSWISE)
				{
					po.x = GetDataDrawRect().left + (m_ValueRatio*(p->m_value.Values[i] - m_MinValue));
					po.y = GetDataDrawRect().bottom - (m_PullOver ? 0 : m_YSignRatio) - CurrentDrawRect.Height() / (float)Count*i;
				}

				Points.Add(po);

				//曲线或者填充 需要使用pos数据
				if (m_IsFill || m_IsCurve)
				{
					Gdiplus::PointF Pof;
					Pof.X = (float)po.x;
					Pof.Y = (float)po.y;
					pos.Add(Pof);
				}
			}

			if (!m_IsCurve&&Points.GetCount()>1)
			{
				CAutoRefPtr<SOUI::IPen> Pen;
				pRT->CreatePen(PS_SOLID, p->m_value.LineColor, m_LineWidth, &Pen);
				pRT->SelectObject(Pen);
				pRT->DrawLines(&Points.GetAt(0), Points.GetCount());
			}
			else
			{
				////曲线的绘制
				Gdiplus::Pen curPen(Gdiplus::Color(GetAValue(p->m_value.LineColor), GetRValue(p->m_value.LineColor), GetGValue(p->m_value.LineColor), GetBValue(p->m_value.LineColor)), m_LineWidth);

				//old
				//Path.AddCurve(&pos[0],pos.GetCount(), p->m_value.Tension);

				//new begin 2020-06-18 优化曲线超过底部的绘制
				float Last;
				float Cur;
				int CurveStart = 0;
				int CurveCount = 0;
				for (int i = 0; i < pos.GetCount(); i++)
				{
					float v = (m_AxisType == AXIS_LENGTHWAYS) ? pos[i].Y : pos[i].X;
					if (i == 0)
						Last = Cur = v;
					Cur = v;

					if (Cur != Last || v < GetDataDrawRect().bottom)
					{
						//曲线累加
						CurveCount++;
						if (i == pos.GetCount() - 1)
							Path.AddCurve(&pos[CurveStart], CurveCount, p->m_value.Tension);
					}
					else
					{
						//曲线结束
						Path.AddCurve(&pos[CurveStart], CurveCount, p->m_value.Tension);
						//绘制线段
						if (Last == Cur&&v >= GetDataDrawRect().bottom)
							Path.AddLine(pos[(i - 1) < 0 ? 0 : (i - 1)], pos[i]);

						//记录曲线开始
						CurveStart = i;
						CurveCount = 1;
					}
					Last = Cur;
				}
				//new end 

				gps.DrawPath(&curPen, &Path);

			}
			//数据点的绘制
			if (m_DataPointSize > 0)
			{
				CAutoRefPtr<SOUI::IPen> pPen;
				pRT->CreatePen(PS_SOLID, p->m_value.LineColor, m_LineWidth, &pPen);
				pRT->SelectObject(pPen);

				CAutoRefPtr<SOUI::IBrush> pBrush;
				CAutoRefPtr<SOUI::IBrush> poldBrush;
				poldBrush = (IBrush*)pRT->GetCurrentObject(OT_BRUSH);
				pRT->CreateSolidColorBrush(m_DataPointCentreColor == RGBA(0, 0, 0, 0) ? p->m_value.LineColor : m_DataPointCentreColor, &pBrush);
				pRT->SelectObject(pBrush);
				for (size_t i = 0; i < Points.GetCount(); i++)
				{
					CRect Rect;
					Rect.left = Points[i].x - m_DataPointSize / 2;
					Rect.top = Points[i].y - m_DataPointSize / 2;
					Rect.bottom = Rect.top + m_DataPointSize;
					Rect.right = Rect.left + m_DataPointSize;
					pRT->FillSolidEllipse(Rect, p->m_value.LineColor);
				}
				pRT->SelectObject(poldBrush);
			}
			//绘制填充
			if (m_IsFill/*&&(!m_IsCurve)*/)
			{
				//闭合线段
				if (m_AxisType == AXIS_LENGTHWAYS)
				{
					Gdiplus::PointF Pof;
					Pof.X = pos[pos.GetCount() - 1].X;
					Pof.Y = m_DataRect.bottom;
					pos.Add(Pof);
					Pof.X = pos[0].X;
					Pof.Y = m_DataRect.bottom;
					pos.Add(Pof);
					Pof.X = pos[0].X;
					Pof.Y = pos[0].Y;
					pos.Add(Pof);
				}
				else if (m_AxisType == AXIS_CROSSWISE)
				{
					Gdiplus::PointF Pof;
					Pof.X = m_DataRect.left;
					Pof.Y = pos[pos.GetCount() - 1].Y;
					pos.Add(Pof);
					Pof.X = m_DataRect.left;
					Pof.Y = pos[0].Y;
					pos.Add(Pof);
					Pof.X = pos[0].X;
					Pof.Y = pos[0].Y;
					pos.Add(Pof);
				}

				Gdiplus::Color C1(GetAValue(p->m_value.FillColor1), GetRValue(p->m_value.FillColor1), GetGValue(p->m_value.FillColor1), GetBValue(p->m_value.FillColor1));
				Gdiplus::Color C2(GetAValue(p->m_value.FillColor2), GetRValue(p->m_value.FillColor2), GetGValue(p->m_value.FillColor2), GetBValue(p->m_value.FillColor2));
				Gdiplus::RectF Rectf;
				Rectf.X = m_DataRect.left;
				Rectf.Y = m_DataRect.top;
				Rectf.Width = m_DataRect.Width();
				Rectf.Height = m_DataRect.Height();
				Gdiplus::LinearGradientBrush blackBrush(Rectf, C1, C2, p->m_value.Angle, false);

				//折线图
				if (!m_IsCurve)
				{
					//绘制闭合区域
					gps.FillPolygon(&blackBrush, &pos[0], pos.GetCount());
				}
				else
				{
					//曲线图
					//Gdiplus::GraphicsPath Path;

					//添加曲线
					//Path.AddCurve(&pos[0], pos.GetCount()-3, p->m_value.Tension);
					Path.AddLines(&pos[pos.GetCount() - 3], 3);
					gps.FillPath(&blackBrush, &Path);
				}
			}

			Pointsx.Add(Points);
		}

		//对于鼠标的响应 //线上的点需要对齐才能生效
		if (CountAligned&&m_CursorTip&&m_CursorPoint.x != 0 && m_CursorPoint.y != 0)
		{
			m_CursorInCoord = false;

			bool DrawRectCheck = CurrentDrawRect.bottom > CurrentDrawRect.top&&CurrentDrawRect.left < CurrentDrawRect.right;
			if (DrawRectCheck&&CurrentDrawRect.PtInRect(m_CursorPoint))
			{
				int index = 0;
				CPoint CursorLinePos[2];
				//绘制一条跟随光标的线
				if (m_AxisType == AXIS_LENGTHWAYS)
				{

					AlignedCount--;
					float space = (CurrentDrawRect.Width() / ((float)AlignedCount == 0 ? 1.f : (float)AlignedCount));
					index = ((m_CursorPoint.x + (space / 2) - CurrentDrawRect.left) / (space));

					CursorLinePos[1].x = CursorLinePos[0].x = CurrentDrawRect.left + index* space;
					CursorLinePos[0].y = CurrentDrawRect.top;
					CursorLinePos[1].y = CurrentDrawRect.bottom;
				}
				else if (m_AxisType == AXIS_CROSSWISE)
				{
					AlignedCount--;
					float space = (CurrentDrawRect.Height() / (float)AlignedCount);
					index = ((CurrentDrawRect.bottom - (m_CursorPoint.y - space / 2)) / space);

					CursorLinePos[1].y = CursorLinePos[0].y = CurrentDrawRect.bottom - index* space;
					CursorLinePos[0].x = CurrentDrawRect.left;
					CursorLinePos[1].x = CurrentDrawRect.right;
				}
				CAutoRefPtr<SOUI::IPen> pPen;
				pRT->CreatePen(PS_SOLID, m_CursorLineColor, m_CursorLineWidth, &pPen);
				pRT->SelectObject(pPen);
				pRT->DrawLines(CursorLinePos, 2);

				//准备绘制提示框
				CRect rect;
				rect.top = m_CursorPoint.y + 15;
				rect.left = m_CursorPoint.x + 15;

				SOUI::SPOSITION pos = m_Data.GetStartPosition();
				int MaxTextWidth = 0;
				int MaxTextHeight = 0;
				SArray<SStringT> TipTexts;
				int n = 0;
				SIZE size;
				while (pos)
				{
					CoordLineData::CPair *p = m_Data.GetNext(pos);

					if (p->m_value.Values.GetCount() == 0)
						continue;

					SStringT TitleStr;
					SIZE TitleSize;

					SStringT str;
					SStringT strValue;

					//修正index 可能会越界的BUG
					index = index >= p->m_value.Values.GetCount() ? p->m_value.Values.GetCount() - 1 : index;
					NumberToScaleStr(p->m_value.Values[index], m_Decimal, strValue);


					if (p->m_value.Texts[index].GetLength() > 0)
					{
						TitleStr.Format(L"%s", p->m_value.Texts[index]);
						TipTexts.Add(TitleStr);
					}

					str.Format(L"%s:%s", p->m_value.Name, strValue);
					TipTexts.Add(str);

					pRT->MeasureText(TitleStr, TitleStr.GetLength(), &TitleSize);
					pRT->MeasureText(str, str.GetLength(), &size);

					MaxTextWidth = max(max(MaxTextWidth, TitleSize.cx), size.cx);
					MaxTextHeight += (TitleStr.GetLength() > 0 ? TitleSize.cy : 0) + size.cy; //标题为空不并入计算

					//数据点放大显示
					CAutoRefPtr<SOUI::IBrush> pBrush;
					CAutoRefPtr<SOUI::IPen> pPen;
					pRT->CreateSolidColorBrush(m_DataPointCentreColor == RGBA(0, 0, 0, 0) ? p->m_value.LineColor : m_DataPointCentreColor, &pBrush);
					pRT->CreatePen(PS_SOLID, p->m_value.LineColor, 1, &pPen);
					pRT->SelectObject(pPen);
					pRT->SelectObject(pBrush);
					CRect Rect;
					Rect.left = Pointsx[n][index].x - (m_DataPointSize*1.8f) / 2.f;
					Rect.top = Pointsx[n][index].y - (m_DataPointSize*1.8f) / 2.f;
					Rect.bottom = Rect.top + m_DataPointSize*1.8f;
					Rect.right = Rect.left + m_DataPointSize*1.8f;
					pRT->FillSolidEllipse(Rect, p->m_value.LineColor);
					n++;
				}

				rect.bottom = rect.top + MaxTextHeight + TIP_MARGIN * 2;
				rect.right = rect.left + MaxTextWidth + TIP_MARGIN * 2;

				//调整提示框位置
				if (rect.bottom > rcClient.bottom) rect.OffsetRect(CPoint(0, -(rect.bottom - rcClient.bottom)));
				if (rect.right > rcClient.right) rect.OffsetRect(CPoint(-(rect.right - rcClient.right), 0));

				//绘制提示框
				pRT->FillSolidRoundRect(rect, CPoint(2, 2), m_CursorTipColorBkgnd);
				if (m_CursorTipFont.GetFontPtr() != NULL)	pRT->SelectObject(m_CursorTipFont.GetFontPtr());
				pRT->SetTextColor(m_CursorTipTextColor);

				//绘制提示框中的文字
				rect.left += TIP_MARGIN;
				rect.top += TIP_MARGIN;
				for (size_t i = 0; i < TipTexts.GetCount(); i++)
				{
					pRT->DrawText(TipTexts[i], TipTexts[i].GetLength(), rect, DT_LEFT | DT_VCENTER /*| DT_SINGLELINE*/);
					rect.OffsetRect(0, size.cy);
				}

				m_CursorInCoord = true;
			}
		}
		pRT->ReleaseDC(hdc);
		AfterPaint(pRT, painter);
	}

	void SLineChart::GetMaxMin(float &Max, float &Min)
	{
		SOUI::SPOSITION pos = m_Data.GetStartPosition();
		bool flag = false;
		while (pos)
		{
			CoordLineData::CPair *p = m_Data.GetNext(pos);
			for (size_t i = 0; i < p->m_value.Values.GetCount(); i++)
			{
				float TempValue = p->m_value.Values[i];
				if (i == 0 && !flag)
				{
					Max = Min = TempValue;
					flag = true;
				}

				if (Min > TempValue)
					Min = TempValue;
				if (Max < TempValue)
					Max = TempValue;
			}
		}
	}


	void SLineChart::SetLineWidth(int Width)
	{
		m_LineWidth = Width;
	}

	void SLineChart::SetFill(int IsFill)
	{
		m_IsFill = IsFill;
	}

	void SLineChart::SetDataEnd(float End)
	{
		m_DataEnd = End;
	}

	void SLineChart::SetCurveChart(bool Curve)
	{
		m_IsCurve = Curve;
	}

	void SLineChart::SetLineTension(int id, float Tension)
	{
		m_Data[id].Tension = Tension;
	}

	void SLineChart::SetLineValue(int id, int index, float Value)
	{
		m_Data[id].Values[index] = Value;
	}

	void SLineChart::SetLineText(int id, int index, SStringT Text)
	{
		m_Data[id].Texts[index] = Text;
	}

	SLineChart::LineTag &SLineChart::GetLineData(int id)
	{
		return m_Data[id];
	}

	void SLineChart::PopTopPushBack(int id, float Value, SStringT Text)
	{
		for (size_t i = 0; i < m_Data[id].Values.GetCount(); i++)
		{
			if (i > 0)
			{
				m_Data[id].Values[i - 1] = m_Data[id].Values[i];
				m_Data[id].Texts[i - 1] = m_Data[id].Texts[i];
			}
		}
		m_Data[id].Values[m_Data[id].Values.GetCount() - 1] = Value;
		m_Data[id].Texts[m_Data[id].Texts.GetCount() - 1] = Text;
	}
}