#include "stdafx.h"
#include "SLineBarChart.h"

namespace SOUI
{

	SLineBarChart::SLineBarChart():SBarChart(),
		m_LineColor(RGBA(255,0,0,255)),
		m_ShowLineValue(true),
		m_LineTextColor(RGBA(0,0,0,0)),
		hasRightAxis(true),
		m_LineDecimal(0),
		m_LineWidth(1)
	{
		SetCoordMargin(CRect(50,50,60,50));
	}


	SLineBarChart::~SLineBarChart()
	{

	}

	void SLineBarChart::AddLineData(int id, float Value)
	{
		if (m_LineData[id].GetCount() > 0)
			AddLineData(id, Value, m_LineData[id][m_LineData[id].GetCount() - 1].Color);
		else
			AddLineData(id, Value, m_LineColor);
	}

	void SLineBarChart::AddLineData(int id, float Value, COLORREF Color)
	{
		BarDataType t;
		t.Value = Value;
		t.Color = Color;
		m_LineData[id].Add(t);
	}

	void SLineBarChart::SetLineData(int id, SArray<BarDataType> &Data)
	{
		m_LineData[id] = Data;
	}

	SOUI::SArray<SOUI::SBarChart::BarDataType> & SLineBarChart::GetLineData(int id)
	{
		return m_LineData[id];
	}

	void SLineBarChart::ClearLineData(void)
	{
		m_LineData.RemoveAll();
	}

	bool SLineBarChart::RemoveLineData(int id)
	{
		return m_LineData.RemoveKey(id);
	}

	void SLineBarChart::SetPercent(bool IsPercent/*=false*/)
	{
		m_IsPercent = IsPercent;
	}

	void SLineBarChart::ShowValue(bool Show)
	{
		m_ShowLineValue = Show;
	}

	void SLineBarChart::OnPaint(IRenderTarget *pRT)
	{
		__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);

		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
		COLORREF curTextColor=pRT->GetTextColor();

		//在右侧画一个轴
		if(m_LineData.GetCount()>0)hasRightAxis=true;
		if(hasRightAxis)
		{
			//计算矩形
			CRect rcClient = GetClientRect();
			CRect rectDraw = rcClient;

			//绘制坐标轴
			int m_YSignRatio;
			int maxHeight;
			{
				CRect Margin = m_CoordMargin;
				POINT Coord[3];
				Coord[0].x = rectDraw.right-Margin.right+10;
				Coord[0].y = rectDraw.bottom - Margin.bottom;
				Coord[1].x = rectDraw.right - Margin.right;
				Coord[1].y = Coord[0].y;
				Coord[2].x = Coord[1].x;
				Coord[2].y = rectDraw.top + Margin.top;

				CAutoRefPtr<SOUI::IPen> Pen;
				pRT->CreatePen(PS_SOLID, m_CoordColor, m_CoordWidth, &Pen);
				pRT->SelectObject(Pen);
				pRT->DrawLines(Coord,3);

				maxHeight = Coord[1].y - Coord[2].y;

				m_YSignRatio = maxHeight / m_ScaleNumber;
			}
			//求最小最大值
			float minVal=FLT_MAX,maxVal=FLT_MIN;
			{
				SOUI::SPOSITION pos = m_LineData.GetStartPosition();
				while (pos)
				{
					CoordDataType::CPair *p = m_LineData.GetNext(pos);
					for(size_t i=0;i<p->m_value.GetCount();i++)
					{
						//计算点的坐标
						if( p->m_value[i].Value < minVal)
						{
							minVal = p->m_value[i].Value;
						}
						if(p->m_value[i].Value > maxVal)
						{
							maxVal = p->m_value[i].Value;
						}
					}
				}
			}
			float maxLen = maxVal - minVal;
			maxVal += maxLen*0.15;
			minVal -= maxLen*0.15;
			maxLen = maxVal - minVal;//最小最大范围各扩展15%

			//绘制Y轴上的刻度
			float m_ValueSpace = maxLen / m_ScaleNumber;
			for(int i=0;maxLen>0.001 && i<m_ScaleNumber+1;i++)
			{
				POINT Coord[2] = { 0 };
				Coord[0].x = m_DataRect.right;
				Coord[0].y = m_DataRect.bottom - m_YSignRatio*(i);
				Coord[1].x = Coord[0].x + 10;
				Coord[1].y = Coord[0].y;
				//画刻度线
				pRT->DrawLines(Coord, 2);
				//画文字
				SStringT ss;
				if(m_IsPercent)
				{
					if(m_LineDecimal>0)
						ss.Format(SStringT().Format(_T("%%.%df%%"),m_LineDecimal),(m_ValueSpace*i+minVal)*100);
					else
						ss.Format(_T("%d%%"),int((m_ValueSpace*i+minVal)*100));
				}
				else
				{
					if(m_LineDecimal>0)
						ss.Format(SStringT().Format(_T("%%.%df"),m_LineDecimal),(m_ValueSpace*i+minVal));
					else
						ss.Format(_T("%d"),int(m_ValueSpace*i+minVal));
				}
				SIZE size;
				pRT->MeasureText(ss, ss.GetLength(), &size);
				pRT->TextOut(Coord[0].x +10 + 5, Coord[0].y - size.cy / 2, ss, -1);
			}
			//绘制折线及文字
			SOUI::SPOSITION pos = m_LineData.GetStartPosition();
			int index=0;
			while (pos)
			{
				CoordDataType::CPair *p = m_LineData.GetNext(pos);
				POINT lastPt;
				size_t count = p->m_value.GetCount();
				for(size_t i=0;i<count && count<=m_arrX.size();i++)
				{
					//计算点的坐标
					float value = p->m_value[i].Value;
					POINT pt;
					pt.x = *(m_arrX.begin()+i);
					pt.y = m_DataRect.bottom - maxHeight * (value-minVal)/maxLen;
					//
					if(i>0)
					{
						POINT Coord[2] = { 0 };
						Coord[0] = lastPt;
						Coord[1] = pt;

						CAutoRefPtr<SOUI::IPen> Pen;
						pRT->CreatePen(PS_SOLID, p->m_value[i].Color, m_LineWidth, &Pen);//绘制折线
						pRT->SelectObject(Pen);
						pRT->DrawLines(Coord, 2);
					}
					lastPt = pt;

					//绘制文字
					if(m_ShowLineValue)
					{
						pt.y -= 5;
						SStringT ss;
						if(m_IsPercent)
						{
							if(m_LineDecimal>0)
								ss.Format(SStringT().Format(_T("%%.%df%%"),m_LineDecimal),value*100);
							else
								ss.Format(_T("%d%%"),int(value*100));
						}
						else
						{
							if(m_LineDecimal>0)
								ss.Format(SStringT().Format(_T("%%.%df"),m_LineDecimal),value);
							else
								ss.Format(_T("%d"),int(value));
						}
						SIZE size;
						pRT->MeasureText(ss, ss.GetLength(), &size);
						pRT->TextOut(pt.x - size.cx/2, pt.y - size.cy / 2, ss, -1);
					}
				}
			}
			
		}

		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		pRT->SetTextColor(curTextColor);

		AfterPaint(pRT, painter);
	}

}