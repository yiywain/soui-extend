/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file		  SChinaMap.h
* @brief
* @version    v1.0
* @author     yiywain(逸远)
* @date       2020-05-28
* gitee  https://gitee.com/yiywain/soui-extend.git
* Describe   坐标系基础类
*/

#pragma once
#include <vector>

namespace SOUI
{

class SChinaMap : public SWindow
{
	SOUI_CLASS_NAME(SChinaMap, L"chinaMap")
public:
	SChinaMap();
	virtual ~SChinaMap();

	void SetMapRect(CRect &Rect);

	struct CityGPSData{
		SStringW CityName;
		float Longitude;//经度 X
		float Latitude;//纬度 Y
		CityGPSData()
		{
			CityName = L"";
			Longitude = 0;
			Latitude = 0;
		}
		CityGPSData(SStringW CityName, float Longitude, float Latitude)
		{
			this->CityName = CityName;
			this->Longitude = Longitude;
			this->Latitude = Latitude;
		}
	};

	struct SplashesData
	{
		friend SChinaMap;
		SStringW CityName;
		int Value;
		COLORREF Color;
		SplashesData()
		{
			CityName = L"";
			Value = 0;
			Color = RGBA(0X59, 0X9E, 0XF6, 255);
		}
		SplashesData(SStringW CityName, int Value, COLORREF Color = RGBA(0X59, 0X9E, 0XF6, 255))
		{
			this->CityName = CityName;
			this->Value = Value;
			this->Color = Color;
		}
	private:
		int Index;//对应的地图数据索引
	};

	bool Add(SplashesData &Data);
	void Clear(void);
	int Size(void);
	
	SplashesData* GetAt(SStringW &CityName);
	void SetHeatPoRange(float Min, float Max);

	void SetLatitudeAmend(float Min,float Max);
	void UpdateWindow()
	{
		InvalidateRect(&GetClientRect());
	}
protected:
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void OnPaint(IRenderTarget *pRT);
private:
	

protected:
	SOUI_MSG_MAP_BEGIN()
		MSG_WM_CREATE(OnCreate)
		MSG_WM_PAINT_EX(OnPaint)
	SOUI_MSG_MAP_END()

	SOUI_ATTRS_BEGIN()
		ATTR_RECT(L"mapMargin", m_MapMargin, TRUE)
		ATTR_STRINGW(L"mapName", m_MapName, TRUE)
		ATTR_FLOAT(L"heatPoMaxSize", m_HeatPoMaxSize, TRUE)
		ATTR_FLOAT(L"heatPoMinSize", m_HeatPoMinSize, TRUE)
		ATTR_BOOL(L"showCity", m_ShowCity,TRUE)
	SOUI_ATTRS_END()
protected:
	CRect m_MapMargin;	//中国的边距 (左边界 上边界 右边界 下边界(到海南))
	SStringW m_MapName;	//地图数据资源名称
	FLOAT m_HeatPoMaxSize; //点最大大小 支持百分比 (0.9为0.9倍的地图绘制区域大小) 建议为0.08
	FLOAT m_HeatPoMinSize; //点最小大小 支持百分比 (0.9为0.9倍的地图绘制区域大小) 建议为0.02
	BOOL m_ShowCity;//显示城市名称

	SArray<CityGPSData> m_CityArray; //城市地理位置数据

	SArray<SplashesData> m_Data; //

private:
	float m_MinAmend;
	float m_MaxAmend;
};


}