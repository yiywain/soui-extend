#include "stdafx.h"
#include "SEllipseChart.h"
#include <GdiPlus.h>
#pragma comment(lib,"gdiplus")

namespace SOUI
{

	SEllipseChart::SEllipseChart() :
		m_DefEllipseBeginColor(RGBA(13, 154, 162, 255))
	{
		m_PullOver = false;


	}


	SEllipseChart::~SEllipseChart()
	{

	}

	void SEllipseChart::AddData(int id, float Value, float Width)
	{
		COLORREF Color = RGBA(GetRValue(m_DefEllipseBeginColor), GetGValue(m_DefEllipseBeginColor), GetBValue(m_DefEllipseBeginColor), 0);
		AddData(id, EllipseDataType(Value, Width, m_DefEllipseBeginColor, Color));
	}

	void SEllipseChart::AddData(int id, EllipseDataType &Data)
	{
		m_Data[id] = Data;
	}



	void SEllipseChart::Clear(void)
	{
		m_Data.RemoveAll();
	}

	bool SEllipseChart::Remove(int id)
	{
		return m_Data.RemoveKey(id);
	}

	void SEllipseChart::OnPaint(IRenderTarget *pRT)
	{
		__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);
		HDC hDC = pRT->GetDC();
		Gdiplus::Graphics graph(hDC);
		graph.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);

		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
		COLORREF curTextColor = pRT->GetTextColor();

		SOUI::SPOSITION pos = m_Data.GetStartPosition();

		int Count = m_Data.GetCount();
		int index = 0;
		while (pos&&!m_PullOver)
		{
			CoordDataType::CPair *p = m_Data.GetNext(pos);
			Gdiplus::RectF Rect;

			Rect.Height = (m_ValueRatio*p->m_value.Value * 2);
			Rect.Y = m_DataRect.bottom - Rect.Height / 2 - m_CoordWidth;

			Rect.Width = p->m_value.Width <= 1 ? m_DataRect.Width()*p->m_value.Width : p->m_value.Width;
			Rect.X = m_DataRect.left + m_XSignRatio*(index + 1) - Rect.Width / 2;

			//对椭圆的绘制区域进行限制
			if (Rect.X < m_DataRect.left) Rect.Offset(m_DataRect.left - Rect.X, 0);
			if (Rect.GetRight() >m_DataRect.right) Rect.Offset(m_DataRect.right - Rect.GetRight(), 0);
			if (Rect.Width > m_DataRect.Width()){
				Rect.Width = m_DataRect.Width();
				Rect.X = m_DataRect.left;
			}

			Gdiplus::Point po[2];
			po[0].X = m_DataRect.left;
			po[0].Y = Rect.Y;
			po[1].X = m_DataRect.left;
			po[1].Y = m_DataRect.bottom;

			Gdiplus::LinearGradientBrush Brush(po[0], po[1], Gdiplus::Color(GetAValue(p->m_value.BeginColor), GetRValue(p->m_value.BeginColor), GetGValue(p->m_value.BeginColor), GetBValue(p->m_value.BeginColor)),
				Gdiplus::Color(GetAValue(p->m_value.EndColor), GetRValue(p->m_value.EndColor), GetGValue(p->m_value.EndColor), GetBValue(p->m_value.EndColor)));

			graph.FillPie(&Brush, Rect, 180, 180);

			index++;
		}
		pRT->ReleaseDC(hDC);

		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		pRT->SetTextColor(curTextColor);

		AfterPaint(pRT, painter);
	}



	void SEllipseChart::GetMaxMin(float &Max, float &Min)
	{
		SOUI::SPOSITION pos = m_Data.GetStartPosition();
		bool flag = false;
		while (pos)
		{
			CoordDataType::CPair *p = m_Data.GetNext(pos);
			float TempValue = p->m_value.Value;
			if (flag)
			{
				Max = Min = TempValue;
				flag = true;
			}
			if (Min > TempValue)
				Min = TempValue;
			if (Max < TempValue)
				Max = TempValue;
		}
	}

	void SEllipseChart::SetData(int id, EllipseDataType &Data)
	{
		m_Data[id] = Data;
	}

	SEllipseChart::EllipseDataType &SEllipseChart::GetData(int id)
	{
		return m_Data[id];
	}

}